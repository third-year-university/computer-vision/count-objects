import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from skimage.morphology import binary_closing, binary_dilation, binary_erosion, binary_opening
from skimage.measure import label

type1 = np.array([[0, 0, 0, 0, 0, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 0, 0, 1, 1, 0],
                  [0, 0, 0, 1, 1, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0],
                  ])

type2 = np.array([[0, 0, 0, 0, 0, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 1, 1, 0, 0, 0],
                  [0, 1, 1, 0, 0, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0],
                  ])

type3 = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0],
                  ])

type4 = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 0, 0, 1, 1, 0],
                  [0, 1, 1, 0, 0, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0],
                  ])

type5 = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 1, 1, 0, 0, 1, 1, 0],
                  [0, 1, 1, 0, 0, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 1, 1, 1, 1, 1, 1, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0],
                  ])

matplotlib.use('TkAgg')

types = [type1, type2, type3, type4, type5]
types_str = ["type1", "type2", "type3", "type4", "type5"]

for i in range(len(types)):
    plt.title(types_str[i])
    plt.imshow(types[i])
    plt.savefig("types/" + types_str[i] + ".png")

image = np.load("ps.npy.txt")
print(f"Total: {label(image).max()}")
print(f"Type {1}: {label(binary_opening(image, type1)).max()}")
print(f"Type {2}: {label(binary_opening(image, type2)).max()}")
print(f"Type {3}: {label(binary_opening(image, type3)).max()}")
print(f"Type {4}: {label(binary_opening(image, type4)).max() - label(binary_opening(image, type3)).max()}")
print(f"Type {5}: {label(binary_opening(image, type5)).max() - label(binary_opening(image, type3)).max()}")
